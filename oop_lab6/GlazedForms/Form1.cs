﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GlazedForms
{
    public partial class Form1 : Form
    {
        public const int WM_NCLBUTTONDOWN = 0xA1;
        public const int HT_CAPTION = 0x2;

        [System.Runtime.InteropServices.DllImportAttribute("user32.dll")]
        public static extern int SendMessage(IntPtr hWnd, int Msg, int wParam, int lParam);
        [System.Runtime.InteropServices.DllImportAttribute("user32.dll")]
        public static extern bool ReleaseCapture();

        public Form1()
        {
            InitializeComponent();
            labelValue.Visible = false;
            label5.Visible = false;
        }
        private void Form1_MouseDown(object sender, System.Windows.Forms.MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                ReleaseCapture();
                SendMessage(Handle, WM_NCLBUTTONDOWN, HT_CAPTION, 0);
            }
        }
        private void Form1_Load(object sender, EventArgs e)
        {
            comboMaterial.SelectedIndex = 0;
        }

        private void buttonCount_Click(object sender, EventArgs e)
        {
            double valueCost;
            valueCost = Convert.ToDouble(textBoxHeight.Text) * Convert.ToDouble(textBoxWidth.Text);
            if (comboMaterial.SelectedIndex == 0 && radioButton1.Checked == true) valueCost *= 0.25;
            if (comboMaterial.SelectedIndex == 0 && radioButton2.Checked == true) valueCost *= 0.3;
            if (comboMaterial.SelectedIndex == 1 && radioButton1.Checked == true) valueCost *= 0.05;
            if (comboMaterial.SelectedIndex == 1 && radioButton2.Checked == true) valueCost *= 0.1;
            if (comboMaterial.SelectedIndex == 2 && radioButton1.Checked == true) valueCost *= 0.15;
            if (comboMaterial.SelectedIndex == 2 && radioButton2.Checked == true) valueCost *= 0.2;
            if (checkBoxDop.Checked == true) valueCost += 35;
            labelValue.Text = Convert.ToString(valueCost) + "Грн";
            labelValue.Visible = true;
            label5.Visible = true;
        }

        private void closeLabel_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
